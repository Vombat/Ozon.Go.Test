step = 1
capacity = 10
pool = []
result = []

const getRandomTime = () => {
    min = Math.ceil(0);
    max = Math.floor(10);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const fullUpParkingLot = () => {
    for (i = 1; i <= capacity; i++) {
        value = getRandomTime(),
            pool.push({
                start: value,
                end: value + getRandomTime()
            })
    }
}

fullUpParkingLot()
const minStartTime = Math.min(...pool.map(value => value.start))
const maxEndTime = Math.max(...pool.map(value => value.end))
console.log(pool)
console.log(`min start time - ${minStartTime}`)
console.log(`max end time - ${maxEndTime}`)
for (time = minStartTime; time < maxEndTime + 1; time++) {

    // count = pool.filter(currentTimeRange => time >= currentTimeRange.start && currentTimeRange.end >= time)
    count = pool.reduce((accumualtor, currentTimeRange) => {
        if (time >= currentTimeRange.start && currentTimeRange.end >= time) accumualtor++
        return accumualtor
    }, 1)

    // console.log('count - ', count.length)
    // goal.push(count.length)
    result.push(count)
}
// console.log('Max - ', Math.max(...goal))
maxCars = Math.max(...result)
console.log('Количество машин в конкретный час - ', result)
console.log('Время когда на стоянке было максимальное количество машин - ', result.indexOf(maxCars))
console.log('Максимально количество машин - ', maxCars)